TEMPLATE     = vcapp
TARGET       = TestCarte
CONFIG      += warn_on qt debug_and_release windows console c++11
HEADERS 	+= include/TestCarte.h include/Liste.h include/Maillon.h
SOURCES     += src/main.cpp src/TestCarte.cpp
INCLUDEPATH += lib/ include/
LIBS	    += lib/VisiTest/VisiTest.lib lib/CommunicationFPGA/CommunicationFPGA.lib
QT += widgets
