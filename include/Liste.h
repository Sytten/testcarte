//!  Definition de classes
/**
    \file  liste.h
    \author fuge2710 & harf2203
    \date 2016-02-06
    \version 1.00
  
    Classe Liste<br>
  
    Historique de revision<br>
    V01.00b   08 fev 2016 fuge2701&harf2203  Creation version 2016<br>
*/

#ifndef LISTE_H
#define LISTE_H

/// Determine une Liste...
/** \class Liste
  La classe liste est une classe presque generique <br>
  permettant d'enregistrer des donnes dans une liste chainee. <br>
  L'interface permet d'acceder aux elements et de les supprimer. <br>
  Elle possede aussi des fonctions utiles a toute liste. <br>
*/

#include "Maillon.h"

template<class T>
class Liste
{
	public:
		Liste();
		Liste(Liste const& p_list);
		~Liste();

		bool ajoutDebut(T p_donne);
		bool ajoutFin(T p_donne);
		bool ajoutPosition(T p_donne, int p_position);

		bool enleverDebut();
		bool enleverFin();
		bool enleverPosition(int p_position);
		bool enleverCourrant();
		bool vider();

		bool setCourantFin();
		bool setCourantDebut();

		int positionCourant();
	
		T& operator++();
		T& operator--();
		T& operator+=(T p_donne);

		T& consulteDebut();
		T& consulteFin();
		T& consultePosition(int p_position);
		T& consulteCourant();

		inline bool estVide() const {return m_nombreMaillons==0; }
       	inline int taille() const { return m_nombreMaillons; }

	private:
		Maillon<T>* m_tete;
		Maillon<T>* m_courant;
		Maillon<T>* m_queue;
		int m_nombreMaillons;

		T m_empty;
	
		friend std::ostream& operator<<(std::ostream& p_out, const Liste<T>& p_liste)
		{
			if (p_liste.m_nombreMaillons > 0)
			{
				Maillon<T>* tmp = p_liste.m_tete;
				for (int i = 0; i < p_liste.m_nombreMaillons; ++i)
				{
					p_out << *tmp << endl;
					tmp = tmp->getSuivant();
				}
			}

			return p_out;
		}
};

template<class T>
Liste<T>::Liste() : m_tete(nullptr), m_courant(nullptr), m_queue(nullptr), m_nombreMaillons(0)
{
}

template<class T>
Liste<T>::Liste(Liste const& p_list) : m_tete(p_list.m_tete), m_courant(p_list.m_courant), m_queue(p_list.m_queue), m_nombreMaillons(p_list.m_nombreMaillons)
{
}

template<class T>
Liste<T>::~Liste()
{
	vider();
}

template<class T>
bool Liste<T>::ajoutDebut(T p_donne)
{
	Maillon<T>* nouveau = new Maillon<T>(p_donne, m_tete);

	if (nouveau == nullptr)
		return false;

	m_tete = nouveau;

	if (m_nombreMaillons == 0)
		m_queue = m_courant = m_tete;

	++m_nombreMaillons;

	return true;
}

template<class T>
bool Liste<T>::ajoutFin(T p_donne)
{
	Maillon<T>* nouveau = new Maillon<T>(p_donne);

	if (nouveau == nullptr)
		return false;

	if (m_queue != nullptr)
		m_queue->setSuivant(nouveau);

	m_queue = nouveau;

	if (m_nombreMaillons == 0)
		m_tete = m_courant = m_queue;

	++m_nombreMaillons;

	return true;
}

template<class T>
bool Liste<T>::ajoutPosition(T p_donne, int p_position)
{
	if (p_position <= 0)
		return ajoutDebut(p_donne);

	else if (p_position >= m_nombreMaillons)
		return ajoutFin(p_donne);

	else
	{
		Maillon<T>* nouveau = new Maillon<T>(p_donne);
		Maillon<T>* precedent = m_tete;

		for (int i = 0; i < p_position - 1; ++i)
		{
			precedent = precedent->getSuivant();
		}

		nouveau->setSuivant(precedent->getSuivant());
		precedent->setSuivant(nouveau);
		++m_nombreMaillons;

		return true;
	}
}

template<class T>
bool Liste<T>::enleverDebut()
{
	if (m_nombreMaillons > 0)
	{
		if (m_tete == m_courant)
			m_courant = nullptr;

		Maillon<T>* temp = m_tete;
		m_tete = m_tete->getSuivant();
		delete temp;

		--m_nombreMaillons;

		if (m_nombreMaillons == 0)
			m_queue = nullptr;

		return true;
	}
	return false;
}

template<class T>
bool Liste<T>::enleverFin()
{
	if (m_nombreMaillons > 0)
	{
		if (m_nombreMaillons == 1)
		{
			delete m_tete;
			m_tete = m_queue = m_courant = nullptr;
		}
		else
		{
			if (m_queue == m_courant)
				m_courant == nullptr;

			Maillon<T>* avant_dernier = m_tete;
			for (int i = 0; i < m_nombreMaillons - 1; ++i)
				avant_dernier = avant_dernier->getSuivant();

			// On efface le maillon.
			delete avant_dernier->getSuivant();
			avant_dernier->setSuivant(nullptr);
			m_queue = avant_dernier;
		}
		--m_nombreMaillons;
		return true;
	}
	return false;
}

template<class T>
bool Liste<T>::enleverPosition(int p_position)
{
	if (p_position >= 0 && p_position < m_nombreMaillons)
	{
		if (p_position == 0)
			enleverDebut();
		else
		{
			// On se place sur le précédent.
			Maillon<T>* precedent = m_tete;
			for (int i = 0; i < p_position - 1; ++i)
				precedent = precedent->getSuivant();

			// On retire le maillon.
			Maillon<T>* a_effacer = precedent->getSuivant();
			precedent->setSuivant(a_effacer->getSuivant());

			if (m_courant == a_effacer)
				m_courant = nullptr;

			delete a_effacer;
			--m_nombreMaillons;
		}
		return true;
	}
	return false;
}

template<class T>
bool Liste<T>::vider()
{
	while (m_nombreMaillons > 0)
		enleverDebut();
	return true;
}

template<class T>
bool Liste<T>::setCourantFin()
{
	m_courant = m_queue;
	if (m_courant == nullptr)
		return false;
	return true;
}

template<class T>
bool Liste<T>::setCourantDebut()
{
	m_courant = m_tete;
	if (m_courant == nullptr)
		return false;
	return true;
}

template<class T>
int Liste<T>::positionCourant()
{
	if (m_nombreMaillons > 0)
	{
		if ( m_courant == m_tete)
			return 1;

		Maillon<T>* cible = m_tete;
		for (int i = 0; i < m_nombreMaillons; ++i)
		{
			if (cible->getSuivant() == m_courant)
				return i + 2;
			else
				cible = cible->getSuivant();
		}
	}

	return -1;
}

template<class T>
T& Liste<T>::consulteDebut()
{
	if (m_nombreMaillons == 0)
		return m_empty;

	return m_tete->getDonne();
}

template<class T>
T& Liste<T>::consulteFin()
{
	if (m_nombreMaillons == 0)
		return m_empty;

	return m_queue->getDonne();
}

template<class T>
T& Liste<T>::consultePosition(int p_position)
{
	if (m_nombreMaillons == 0)
		return m_empty;

	if (p_position < 0 || p_position >= m_nombreMaillons)
		return m_empty;

	Maillon<T>* cible = m_tete;
	for (int i = 0; i < p_position; ++i)
	{
		cible = cible->getSuivant();
	}

	return cible->getDonne();
}

template<class T>
T& Liste<T>::consulteCourant()
{
	if (m_courant == nullptr)
		return m_empty;

	return m_courant->getDonne();
}

template<class T>
T& Liste<T>::operator++()
{
	if (m_courant == nullptr)
		m_courant = m_tete;
	else if (m_courant->getSuivant() != nullptr)
		m_courant = m_courant->getSuivant();

	return consulteCourant();
}

template<class T>
T& Liste<T>::operator--()
{
	if (m_nombreMaillons == 1 || m_nombreMaillons == 0)
		return consulteCourant();

	if (m_courant == m_tete || m_courant == nullptr)
		return consulteCourant();

	Maillon<T>* cible = m_tete;
	for (int i = 0; i < m_nombreMaillons; ++i)
	{
		if (cible->getSuivant() == m_courant){
			m_courant = cible;
			break;
		}
		else
			cible = cible->getSuivant();
	}

	return consulteCourant();
}

template<class T>
T& Liste<T>::operator+=(T p_donne)
{
	ajoutFin(p_donne);
	return m_queue->getDonne();
}

#endif