//!  Definition de classes
/**
    \file  Maillon.h
    \author fuge2710 & harf2203
    \date 2016-02-06
    \version 1.00
  
    Classe Maillon<br>
  
    Historique de revision<br>
    V01.00b   08 fev 2016 fuge2701&harf2203  Creation version 2016<br>
*/

#ifndef MAILLON_H
#define MAILLON_H

/// Determine un Maillon...
/** \class Maillon
  La classe Maillon est un simple conteneur <br>
  permettant d'enregistrer une donne dans la liste chainee. <br>
*/

#include<iostream>

template<class T>
class Maillon
{
	public:
		Maillon(T p_donne) : m_donne(p_donne), m_suivant(nullptr) {}
		Maillon(T p_donne, Maillon<T>* p_suivant) : m_donne(p_donne), m_suivant(p_suivant) {}

		Maillon<T>* getSuivant() { return m_suivant; }
		bool setSuivant(Maillon<T>* p_suivant) { m_suivant = p_suivant; return true; }
	
		T& getDonne() { return m_donne;  }
	
	private:
		Maillon<T>* m_suivant;
		T m_donne;
	
		friend std::ostream& operator<<(std::ostream& p_out, const Maillon<T>& p_maillon)
		{
			p_out << p_maillon.m_donne;
			return p_out;
		}
};

#endif