/*
*   La classe MonInterface teste l'interface usager VisiTest (librairie).
*   
*   $Author: bruc2001 $
*   $Date: 2014-12-18 11:43:08 -0500 (jeu., 18 déc. 2014) $
*   $Revision: 103 $
*   $Id: MonInterface.h 103 2014-12-18 16:43:08Z bruc2001 $
*
*   Copyright 2016 Département de génie électrique et génie informatique
*                  Université de Sherbrooke  
*/
#ifndef TESTCARTE_H
#define TESTCARTE_H

#include<iostream>
#include<fstream>
#include<iomanip>
#include<cmath>

#include "Liste.h"
#include "VisiTest/VisiTest.h"
#include "CommunicationFPGA/CommunicationFPGA.h"

class TestCarte : public VisiTest
{
	public:
		TestCarte() = delete;
		TestCarte(const char *theName);
	
		virtual void sauvegarder(char *nomFichier);
	
	public slots:
		virtual void testSuivant();
		virtual void demarrer();
		virtual void arreter();
		virtual void vider();
		virtual void modeFile();
		virtual void modePile();

		virtual void premier();
		virtual void dernier();
		virtual void precedent();
		virtual void suivant();
	
	private:
		bool evenParity(int x);

	private:
		DonneesTest m_donneeCourante;
		CommunicationFPGA m_fpga;
		Liste<DonneesTest> m_archive;
		bool m_enregistrer;
		bool m_isFile;
};

#endif // TESTECARTE_H