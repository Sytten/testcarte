/*
*   Voir fichier d'entête pour informations.
*   
*   $Author: bruc2001 $
*   $Date: 2014-12-18 11:43:08 -0500 (jeu., 18 déc. 2014) $
*   $Revision: 103 $
*   $Id: TestCarte.cpp 103 2014-12-18 16:43:08Z bruc2001 $
*
*   Copyright 2013 Département de génie électrique et génie informatique
*                  Université de Sherbrooke  
*/
#include <QStyleFactory>
#include "TestCarte.h"

using namespace std;

TestCarte::TestCarte(const char * theName) : VisiTest(theName), m_enregistrer(false), m_isFile(true)
{
	m_donneeCourante.typeTest = 1;
	m_donneeCourante.registreSW = SW;
	m_donneeCourante.retourSW = 1;

	m_donneeCourante.registreLD = LD;
	m_donneeCourante.valeurLD = 1;

	m_donneeCourante.etatLD = 1;
	m_donneeCourante.etatSW = 1;

	resetTest();
	resetArchive();
}

void TestCarte::testSuivant()
{
	/*Check if communication OK*/
	if (!m_fpga.estOk())
	{
		cerr << m_fpga.messageErreur() << endl;
		return;
	}

	/*Read the switches*/
	if (!m_fpga.lireRegistre(m_donneeCourante.registreSW, m_donneeCourante.etatSW))
	{
		cerr << m_fpga.messageErreur() << endl;
		return;
	}
	else
		m_donneeCourante.retourSW = m_donneeCourante.etatSW;

	/*Set led output depending on the type of test*/
	if (m_donneeCourante.typeTest == 1)
	{
		m_donneeCourante.valeurLD = m_donneeCourante.retourSW;
		message("Test 1 effectue");
	}
	else if (m_donneeCourante.typeTest == 2)
	{
		if (evenParity(m_donneeCourante.retourSW))
			m_donneeCourante.valeurLD = 255;
		else
			m_donneeCourante.valeurLD = 0;
		message("Test 2 effectue");
	}
	else
	{
		m_donneeCourante.valeurLD = pow(2, (int)log2(m_donneeCourante.retourSW + 1)) - 1;
		message("Test 3 effectue");
	}

	/*Write the state of the led and verify its well done*/
	if (!m_fpga.ecrireRegistre(m_donneeCourante.registreLD, m_donneeCourante.valeurLD))
	{
		cerr << m_fpga.messageErreur() << endl;
		return;
	}
	else
		m_donneeCourante.etatLD = m_donneeCourante.valeurLD;

	/*Write test*/
	setTest(m_donneeCourante);

	/*Save the test if needed*/
	if (m_enregistrer)
	{
		if (m_isFile)
			m_archive += m_donneeCourante;

		else
			m_archive.ajoutDebut(m_donneeCourante);

		setArchive(m_archive.positionCourant(), m_archive.taille());

		if (m_archive.taille() == 1)
			setArchive(m_donneeCourante);
	}

	/*Increment the test number*/
	m_donneeCourante.typeTest++;
	if (m_donneeCourante.typeTest > 3)
		m_donneeCourante.typeTest = 1;
   
}

void TestCarte::sauvegarder(char *nomFichier)
{
	fstream outputFile;
	outputFile.open(nomFichier, std::fstream::out | std::fstream::trunc);

	if (outputFile.is_open())
	{
		outputFile << m_archive;
		outputFile.close();
	}
	else
	{
		cerr << "Error occured when opening the file " << nomFichier << endl;
	}
}

void TestCarte::demarrer()
{
	m_enregistrer = true;
	message("Sauvegarde commencee");
}

void TestCarte::arreter()
{
	m_enregistrer = false;
	message("Sauvegarde terminee");
}

void TestCarte::vider()
{
	m_archive.vider();
	resetArchive();
	message("Archive videe");
}

void TestCarte::modeFile()
{
	if (m_archive.estVide())
	{
		message("Mode file activee");
		m_isFile = true;
	}
	else
		message("Impossible de changer le mode de sauvegarde");
}

void TestCarte::modePile()
{
	if (m_archive.estVide())
	{
		message("Mode pile activee");
		m_isFile = false;
	}
	else
		message("Impossible de changer le mode de sauvegarde");
}

std::ostream& operator<<(std::ostream& p_out, const DonneesTest& p_donne)
{
	p_out << "Type test : "			<< p_donne.typeTest << endl;
	p_out << "Adresse switches : "	<< p_donne.registreSW << endl;
	p_out << "Retour switches : "	<< std::setbase(16) << p_donne.retourSW
									<< std::setbase(10) << " (" << p_donne.retourSW << ")" << endl;
	p_out << "Etat switches : "		<< std::setbase(16) << p_donne.etatSW
									<< std::setbase(10) << " (" << p_donne.etatSW << ")" << endl;
	p_out << "Valeur leds : "		<< p_donne.registreLD << endl;
	p_out << "Etat switches : "		<< std::setbase(16) << p_donne.valeurLD
									<< std::setbase(10) << " (" << p_donne.valeurLD << ")" << endl;
	p_out << "Etat leds : "			<< std::setbase(16) << p_donne.etatLD
									<< std::setbase(10) << " (" << p_donne.etatLD << ")" << endl;
	return p_out;
}

void TestCarte::premier()
{
	if (m_archive.taille() != 0)
	{
		m_archive.setCourantDebut();
		setArchive(m_archive.consulteCourant());
		setArchive(1, m_archive.taille());
	}
}

void TestCarte::dernier()
{
	if (m_archive.taille() != 0)
	{
		m_archive.setCourantFin();
		setArchive(m_archive.consulteCourant());
		setArchive(m_archive.taille(), m_archive.taille());
	}
}

void TestCarte::precedent()
{
	if (m_archive.taille() != 0)
	{
		m_archive--;
		setArchive(m_archive.consulteCourant());
		setArchive(m_archive.positionCourant(), m_archive.taille());
	}
}

void TestCarte::suivant()
{
	if (m_archive.taille() != 0)
	{
		m_archive++;
		setArchive(m_archive.consulteCourant());
		setArchive(m_archive.positionCourant(), m_archive.taille());
	}
}

bool TestCarte::evenParity(int x)
{
	int total = 0, c = 1;

	for (int i = 0; i < 32; i++){
		if (x & (c << i))
			total++;
	}

	if (total % 2)
		return false;

	return true;
}