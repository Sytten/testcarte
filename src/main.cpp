/*
*   Ce programme teste les fonctionnalites de la carte FPGA
*   avec l'aide de la classe TestCarte.
*   
*   $Author: fuge2701 $
*   $Date: 2016-06-03 $
* 
*/
#include <QApplication>
#include "TestCarte.h"

int main( int argc, char ** argv )
{
	QApplication app(argc, argv);
 
	TestCarte gui("TestCarte V1.0");

    return app.exec();
}